/**
  * jQuery.enllax.js v1.1.0
  * https://github.com/mmkjony/enllax.js
  * demo: http://mmkjony.github.io/enllax.js/
  *
  * Copyright 2015, MMK Jony
  * This content is released under the MIT license
 **/

(function($) {
  "use strict";

  $.fn.enllax = function(opt) {
    var options = $.extend(
      {
        ratio: 0,
        type: "background", //foreground
        direction: "vertical" //horizontal
      },
      opt
    );

    var elem = $("[data-enllax-ratio]");

    var winHeight = $(window).height();

    elem.each(function() {
      var ratio;
      var type;
      var dir;
      var $this = $(this);
      var offset = $this.offset().top;
      var height = $this.outerHeight();
      var dataRat = $this.data("enllax-ratio");
      var dataType = $this.data("enllax-type");
      var dataDir = $this.data("enllax-direction");

      // set will-change for performance boost
      $this.css("will-change", "transform");

      if (dataRat) {
        ratio = dataRat;
      } else {
        ratio = options.ratio;
      }

      if (dataType) {
        type = dataType;
      } else {
        type = options.type;
      }

      if (dataDir) {
        dir = dataDir;
      } else {
        dir = options.direction;
      }

      var transform = Math.round((offset - winHeight / 2 + height) * ratio);

      if (type == "foreground") {
        if (dir == "vertical") {
          $this.css({
            "-webkit-transform": "translateY(" + transform + "px)",
            "-moz-transform": "translateY(" + transform + "px)",
            transform: "translateY(" + transform + "px)"
          });
        }
      }

      $(window).on("scroll", function() {
        var offset = $this.offset().top;
        var height = $this.outerHeight();

        var docHeight = Math.max(
          document.body.scrollHeight,
          document.documentElement.scrollHeight,
          document.body.offsetHeight,
          document.documentElement.offsetHeight,
          document.body.clientHeight,
          document.documentElement.clientHeight
        );

        var scrolling = $(this).scrollTop();

        transform = Math.round(
          (offset - winHeight / 2 + height - scrolling) * ratio
        );

        // console.log(
        //   `Offset: ${offset}
        //     winHeight: ${winHeight}
        //     Height: ${height}
        //     scrolling: ${scrolling}
        //     docHeight: ${docHeight}`
        // );

        if (type == "foreground" && scrolling < docHeight) {
          if (dir == "vertical") {
            window.requestAnimationFrame(function() {
              $this.css({
                "-webkit-transform": "translateY(" + transform + "px)",
                "-moz-transform": "translateY(" + transform + "px)",
                transform: "translateY(" + transform + "px)"
              });
            });
          }
        }
      });
    });
  };
})(jQuery);
